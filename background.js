function timeString( t ) {
            var ret = [], t = {
                hr: parseInt(t/3600),
                min: parseInt(t/60%60),
                sec: t%60
            };

            for ( var n in t )
                t[n] && ret.push( t[n], n + "s".substr( t[n] < 2 ) );
            return ret.join(" ");
}

function completeTask(task_status,taskID){
    var stop_task = "UPDATE task_manager SET task_status =? WHERE id =?";
    var dbSize = 5 * 1024 * 1024; // 5MB
    var db = openDatabase("task_manager", "1.0", "Task Manager", dbSize);
    db.transaction(function(tx){
        tx.executeSql(stop_task,
                [task_status, taskID],
                doNothing, onError);
            });
}

function doNothing(){
//    console.log('You called me sir?');
}

function onError(tx, error) {
    jQuery('.tasks_list').html(error.message);
}

var t = null;
var newval = null;
var pauseval = null;
var task = '';

chrome.browserAction.setBadgeText({text:String('Task')});
chrome.browserAction.setBadgeBackgroundColor({color:[0, 200, 0, 100]});

chrome.extension.onRequest.addListener(function(request, sender, sendResponse) {
    var timex = request.durationSecs;

    function timed(m){
        newval = m;
        chrome.extension.sendRequest({secsRemaining: newval--,taskID:request.taskID,task:'running'});
//        chrome.browserAction.onClicked.addListener(function(tab) {
////            chrome.tabs.executeScript(null,{code:'document.createElement("img");'});
////            chrome.tabs.executeScript(null,{code:"document.body.createAttribute('id','div1')"});
////            chrome.tabs.executeScript(null,{code:"document.body.appendChild(overlay);"});
////            chrome.tabs.executeScript(null,{code:"document.body.bgColor='red'"});
//        });
        chrome.browserAction.setBadgeText({text:String(newval)});

        if(newval > 0 ){
            start(newval);
        }else{
            chrome.browserAction.setBadgeText({text:String('done')});
            chrome.extension.sendRequest({task: 'done',taskID:request.taskID});
            completeTask('done',request.taskID);
            
            //set css for coundown
            alert('You ran out of time.');
//            chrome.tabs.executeScript(null,{code:"document.body.bgColor='red'"});
//            var tabdata = chrome.tabs.executeScript(null, { file: "content_script.js" });
            
//            chrome.extension.sendRequest({secsRemaining: newval--,taskID:request.taskID,task:'running'});
            //attempt to run overlay script

        }
    }

    function start(timex){
        t = window.setTimeout(function(){
           timed(timex);
        },1000);
       return t;
    }
    console.log('background task : '+request.task);
    if(request.task === 'start'){
         start(timex);
    }

    if(request.task === 'pause'){
        chrome.browserAction.setBadgeText({text:String('pause')});
        chrome.extension.sendRequest({time:newval,task: 'pause',taskID:request.taskID});
        window.clearInterval(t);
    }

    if(request.task === 'resume'){
        /*
         * Commented: Caused flickering of resume link and doesn't add/remove any functinality by commenting it
         * chrome.extension.sendRequest({time:request.newtime,task: 'pause',taskID:request.taskID}); 
        */
        start(request.newtime);
    }
    
    if(request.task === 'done'){
        chrome.browserAction.setBadgeText({text:String('done')});
        chrome.extension.sendRequest({time:newval,task: 'done',taskID:request.taskID});
//        console.log('task complete in background');
        window.clearInterval(t);
    }
    
    if(request.task === 'active_task'){
        jQuery('#countdown').html(''); //set this here to prevent blank countdown
        chrome.extension.sendRequest({task: 'active_task',taskID:request.taskID});
    }
  });
