	//instatiate some cool stuff here
	var create_tbl = "CREATE TABLE IF NOT EXISTS task_manager (id INTEGER PRIMARY KEY AUTOINCREMENT, task TEXT, task_time TEXT, task_status TEXT,run_status TEXT,pause_time TEXT,pause_freq TEXT, datetime DATETIME)";
	var get_tasks = "SELECT * FROM task_manager";
	var get_task = "SELECT * FROM task_manager WHERE id=?";
        var update_task = "UPDATE task_manager SET task =? WHERE id=?";
        var update_time = "UPDATE task_manager SET task_time =?, pause_time = 0 WHERE id=?";//offset the pause_time as well
	var insert_task = "INSERT INTO task_manager(task, task_time, task_status, run_status, pause_time, datetime) VALUES (?,?,?,?,?,?)";
	var delete_task = "DELETE FROM task_manager WHERE id=?";
	var pause_task = "UPDATE task_manager SET run_status =?,pause_time =?,pause_freq =? WHERE id =?";
	var stop_task = "UPDATE task_manager SET task_status =? WHERE id =?";
	var complete_task = "UPDATE task_manager SET task_status =? WHERE id =?";

        function timeString( t ) {
            var ret = [], t = {
                hr: parseInt(t/3600),
                min: parseInt(t/60%60),
                sec: t%60
            };

            for ( var n in t )
                t[n] && ret.push( t[n], n + "s".substr( t[n] < 2 ) );
            return ret.join(" ");
        }
        jQuery(function(){
            chrome.extension.onRequest.addListener(
            function(request, sender, sendResponse) {
                jQuery('#active_task').val(request.taskID);
                jQuery('#countdown').show();
                jQuery('#done_'+request.taskID).show();
                jQuery('#countdown').html('paused');
                if(request.task === 'pause'){
                    //log that time in the table of tasks
                    pauseTask('pause',request.time,request.taskID);
                    jQuery('#countdown').html('paused');
                }else if(request.task === 'done'){
                    completeTask('done',request.taskID); //stop task incase the popup.html is being viewed.
                    jQuery('#countdown').html('task is done');
                }else if(request.task === 'running'){
                    //disable delete button
                    jQuery('#task_'+request.taskID).find('.start').replaceWith('<span class="start" id="pause">pause</span>');
                }
                jQuery('#curr_time').val(request.secsRemaining);
                if(request.task !== 'pause'){
                    var active_task = parseInt(jQuery('#active_task').val());
                    disable_tasks(active_task);
                    jQuery('#countdown').html(timeString(request.secsRemaining));
                }
            });
        });
        
        disable_tasks = function(id){
            //change to active state
            jQuery('#task_'+id).removeClass('inactive');
            jQuery('#task_'+id).addClass('active');
            
            //get active task delete #id and disable click event
            jQuery('#'+id).click(false); //disable
            jQuery('#'+id).css({'color':'#ccc','cursor':'default'}); //change color
            jQuery('.ttime').click(false); //disable time edit on active task
            
            //add some styling to the inactive tasks
            jQuery('.inactive').css({'background':'#e2e2e2','color':'#ccc'});
            jQuery('.inactive').find('span').css({'color':'#ccc'});
            jQuery('.inactive').find('a').css({'color':'#ccc','cursor':'default'});
            jQuery('.inactive').click(false);
        }
        
        jQuery('#addtask').live('click',function(){
		// get the task and time
		var task = jQuery('#task').val();
                var task_time = jQuery('#task_time').val();

                //check for empty task
                if(jQuery('label')[0]){
                    jQuery('label')[0].remove();
                }
                if(jQuery('label')[1]){
                    jQuery('label')[1].remove();
                }
                if(task === ''){
                    jQuery('<label for="task" class="error">Error: Task name required</label>').insertBefore('#task');
                    jQuery('#task').focus();
                }   
                else if(task_time === ''){
                    jQuery('<label for="time" class="error">Error: Time required<br /></label>').insertBefore('#task_time');
                    jQuery('#task_time').focus();
                }else{
                    addTask(task,task_time);
                }
	});

         jQuery('#edittask').live('click',function(){
		// get the task and time
		var task = jQuery('input[name=task]').val();
		var task_time = jQuery('#task_time').val();
                var task_id = jQuery('input[name=task]').attr('id');
		editTask(task,task_time,task_id);
	});

        jQuery('.done').live('click',function(){
            
                //complete the task
                var task_id = parseInt(jQuery('#active_task').val());
                
                //log completed task
                completeTask('done',task_id);
                
                //change countdown text
                jQuery('#countdown').html('No active tasks');
                
                //strikethrough the task
                jQuery('#task_'+task_id).css({'text-decoration':'line-through'});
                jQuery('#task_'+task_id+' a').css({'text-decoration':'line-through'});
                jQuery('#task_'+task_id+' span').css({'text-decoration':'line-through'});
	});

	  var dbSize = 5 * 1024 * 1024; // 5MB
	  var db = openDatabase("task_manager", "1.0", "Task Manager", dbSize);

	showTasks();
	createTable();

	function onError(tx, error) {
        	jQuery('.tasks_list').html(error.message);
    }

    function createTable(){
	    //create table
	    db.transaction(function(tx) {
	        tx.executeSql(create_tbl, [], showTasks, onError);
	    });
	}

	function showTasks(){
		db.transaction(function(tx) {
			tx.executeSql(get_tasks, [], function(tx, result) {
			var dataset = result.rows;
			var task_html;
			task_html = '<ol class="tasks">';
			for (var i = 0, item = null; i < dataset.length; i++) {
				item = dataset.item(i);
                                //pause time as int
                                var task_time = parseInt(item['task_time']);
                                var pause_time = item['pause_time'];
                                var display_time = parseFloat(pause_time/60);
                                display_time = display_time.toFixed(2);
                                var tasktime = 0;
                                if(display_time > 0){
                                    tasktime = display_time;
                                }else{
                                    tasktime = task_time;
                                }
                                if(item['task_status'] !== 'done' && parseInt(item['pause_time']) !== 0){
                                    task_html += '<li style="border: 1px solid #CCC;" id="task_'+item['id']+'" class="inactive"><span id="task">'+item['task']+'</span><span class="start" id="resume">resume</span><a href="javascript:void(0);" class="done" id="done_'+item['id']+'">done?</a><a class="delete" id="'+item['id']+'" href="javascript:void(0);">delete</a><span style="float:right;margin-right:30px;" class="ttime" id="'+pause_time+'">'+display_time+' min</span></li>';
                                }else if(item['task_status'] === 'done'){
                                    task_html += '<li style="border: 1px solid #CCC;text-decoration:line-through;" id="task_'+item['id']+'" class="inactive"><span id="task">'+item['task']+'</span><span class="start" id="expired">expired</span><a class="delete" id="'+item['id']+'" href="javascript:void(0);">delete</a><span style="float:right;margin-right:30px;text-decoration:line-through;" class="ttime" id="'+item['task_time']+'">'+item['task_time']+' min</span></li>';
                                }else if(parseInt(item['pause_time']) === 0){
                                    task_html += '<li style="border: 1px solid #CCC;" id="task_'+item['id']+'" class="inactive"><span id="task">'+item['task']+'</span><span class="start" id="start">start</span><a href="javascript:void(0);" class="done" id="done_'+item['id']+'">done?</a><a class="delete" id="'+item['id']+'" href="javascript:void(0);">delete</a><span style="float:right;margin-right:30px;" class="ttime" id="'+item['task_time']+'">'+item['task_time']+' min</span></li>';
                                }
			}
			task_html += '</ol>';
			jQuery('.tasks_list').html(task_html);
	 	});
	  });
	}

	//delete selected item
	jQuery('li .delete').live('click',function(e){
			jQuery('#'+e.target.id).parent().css({'background':'#f4cbcb','opacity':0.5});
			setTimeout(function() {jQuery('#'+e.target.id).parent().fadeOut();}, 1000 );
			setTimeout(function() {
			db.transaction(function(tx) {
		          tx.executeSql(delete_task, [e.target.id], showTasks, onError);
		        });
		},2000);
	});

    function addTask(task,task_time){
        jQuery('#task_time').val('');
        jQuery('input[name=task]').val('');
    	db.transaction(function(tx){
            var datetime = new Date();
            var task_status = 0; //status 0 by default
            var run_status = 0; //status 0 by default
            var pause_time = 0; //status 0 by default
            tx.executeSql(insert_task,
                    [task, task_time, task_status, run_status, pause_time, datetime],
                    showTasks, onError);
               });
	  }

    function pauseTask(task_status,task_time,taskID){
        pauseFreq(taskID);
        var freq = parseInt(jQuery('#pause_freq').val());
        //check prev log and increment
        if(freq){
            freq += 1;
        }else{
            freq = 1;
        }
    	db.transaction(function(tx){
            var datetime = new Date();
            var task_status = 0; //status 0 by default
            var run_status = 0; //status 0 by default
            tx.executeSql(pause_task,
                    [task_status, task_time,freq, taskID],
                    showTasks, onError);
               });         
	  }
   
   function pauseFreq(taskID){
       db.transaction(function(tx) {
       tx.executeSql(get_task, [taskID], function(tx, result) {
            //check prev pause time and increment
            var freq = parseFloat(result.rows.item(0).pause_freq);
            jQuery('#pause_freq').val(freq);
            });
       });
    }
    
   function stopTask(task_status,taskID){
    	db.transaction(function(tx){
			tx.executeSql(stop_task,
			        [task_status, taskID],
			        showTasks, onError);
			   });
	  }

   function completeTask(task_status,taskID){
    	db.transaction(function(tx){
			tx.executeSql(complete_task,
			        [task_status, taskID],
			        showTasks, onError);
			   });
	  }

    function editTask(task,task_id){
        db.transaction(function(tx){
            tx.executeSql(update_task,
                    [task,task_id],
                    showTasks, onError);
                });
    }

    function editTime(task_time,task_id){
        console.log(task_time+''+task_id);
        db.transaction(function(tx){
            tx.executeSql(update_time,
                    [task_time,task_id],
                    showTasks, onError);
                });
    }

	  //action on start task execution
	  jQuery('li .start').live('click',function(e){
              var task_time = jQuery(this).nextAll('.ttime').attr('id');
              var task_id = parseInt(jQuery(this).nextAll('.delete').attr('id'));
              task_time = task_time.split(' min');
                task_time = task_time[0]*60;
                if(jQuery(this).html() === 'start'){
                    
                    jQuery('#active_task').val(''); //unset prev task id
                    jQuery('#active_task').val(task_id);
                    
                    jQuery('#start').replaceWith('<span class="start" id="pause">pause</span>');
                    chrome.extension.sendRequest({durationSecs: task_time,task:'start',taskID:task_id});
                }if(jQuery(this).html() === 'pause'){
                    //remove class attr 'active' from paused task
                    jQuery(this).parent().removeClass('active');
                    jQuery(this).parent().addClass('inactive');
                    
                    jQuery(this).replaceWith('<span class="start" id="resume">resume</span>');
                    var new_time = parseInt(jQuery('#curr_time').val());
                    jQuery('#countdown').html(timeString(new_time));
                    chrome.extension.sendRequest({task: 'pause',newtime:new_time,taskID:task_id});
                }if(jQuery(this).html() === 'resume'){
                      
                      jQuery('#active_task').val(''); //unset prev task id
                      jQuery('#active_task').val(task_id);
                      chrome.extension.sendRequest({task: 'active_task',taskID:task_id});
                      
                      var pause_time = jQuery(this).nextAll('.ttime').attr('id');
                      pause_time = pause_time.split(' min');
                      pause_time = parseInt(pause_time);
                      chrome.extension.sendRequest({task: 'resume',newtime:pause_time,taskID:task_id});
                      jQuery(this).replaceWith('<span class="start" id="pause">pause</span>');
                }
	  	});
                
jQuery('li .done').live('click',function(e){
    var task_id = parseInt(jQuery(this).nextAll('.delete').attr('id'));
   if(jQuery(this).html() === 'done?'){
        jQuery(this).replaceWith('<span class="start" id="pause">task is done</span>');
        var done_time = parseInt(jQuery('#curr_time').val());
        jQuery('#countdown').html(timeString(done_time));        
        chrome.extension.sendRequest({task: 'done',newtime:done_time,taskID:task_id});
    } 
});              

jQuery(function(){
      jQuery('li #task').live('click',function(){
          if(jQuery(this).parent().find('.start').html() !== 'expired'){ //id expired, don't delete
                var task_edit = jQuery(this).html();
                var edit_id = jQuery(this).parent().attr('id');
                edit_id = edit_id.split('_');
                jQuery(this).replaceWith('<input type="text" class="inputbox" id="'+edit_id[1]+'" name="task" value="'+task_edit+'" />');
         }else{
             jQuery('#error').html('task is already expired');
         }
      });

      jQuery('li .ttime').live('click',function(){
          if(jQuery(this).parent().find('.start').html() !== 'expired'){ //id expired, don't delete
                var task_time = parseInt(jQuery(this).attr('id'));
                task_time = eval(task_time).toFixed(0);
                task_time = parseInt(task_time);
                var edit_id = jQuery(this).parent().attr('id');
                edit_id = edit_id.split('_');
                jQuery('#time_edit').val(task_time);
                jQuery(this).replaceWith('<input type="text" style="width:70px;margin-left:50px;" class="inputbox" id="'+edit_id[1]+'" name="task_time" value="'+task_time+'" /> min');
            }else{
                jQuery('#error').html('task is already expired');
                setTimeout(function() {
                     jQuery('#error').html('');
                },1000);
         }
      });

    jQuery('li input[name=task]').live('blur',function(){
            var task_id = jQuery(this).attr('id');
            var task = jQuery(this).val();
            editTask(task,task_id);
        });

        jQuery('li input[name=task_time]').live('blur',function(){
            //check prev val and now val
            if(parseInt(jQuery('#time_edit').val()) === parseInt(jQuery(this).val())){
                console.log('same..');
                var display_time = parseInt(jQuery(this).val())/60;
                jQuery(this).replaceWith('<span style="float:right;margin-right:30px;" class="ttime" id="'+parseInt(jQuery(this).val())+'">'+display_time.toFixed(1)+' min</span>');
            }else{
                console.log('not same..');
                var task_id = jQuery(this).attr('id');
                var task_time = jQuery(this).val();
                editTime(task_time,task_id);
            }
        });

//        jQuery('#cancel').live('click',function(){
//            var task_id = jQuery(this).prev().attr('id');
//            task_id = task_id.split('_')[0];
//            var task = jQuery(this).prev().val();
//            jQuery(this).prev().remove();
//            jQuery(this).remove();
//            jQuery('#task_33').append('<span id="task">ewew</span>');
//        });
});